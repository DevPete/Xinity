```
SENG302 Group Project 2016
29th February - 29th September
```

# Xinity
A web based musical application by EchoTech (team 4)

## What is it?
Xinity is a web based musical application for assisting with learning and improvement of various
musical skills and knowledge.

## Running Xinity
1. Package Xinity by navigating to the root folder and running `mvn clean site package`
2. Navigate to the `/target` folder and run `java -jar Xinity-X.X.X.jar`
    * Replace X's with the corresponding version number of `.jar` file.
    * This will start the application with the GUI.
3. You can also execute the application with the following additional parameters;
    * `-server` Start's the application in server mode.
    * `-server -withtestdata` Start's the application in server mode and generates databases
    populated with test data (Caution: Will overwrite any existing database contents!)

## The test data
The `-withtestdata` flag will generate a user with test data (user details, 2 schedules and a few
schedule attempts).

* Username = `test`
* Password = `apples`

## Project Contributors
* Adam Hunt
* Alex Gabites
* Jono Smythe
* Liam McKee
* Matthew Jensen
* Peter Roger
* William Scott
